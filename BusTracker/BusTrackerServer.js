/**
 * Created by hornick for SE2840
 */
class ExpressDemo {
    // Note:
    // The constructor is called when the 'new' statement
    // (at bottom of file) is executed.
    constructor() {
        // do nothing in this case (never runs since ExpressDemo is not initialized)
    }

    // This function can have any name you choose.
    static serve() {

        let express = require('express');           // express web server framework
        let favicon = require('express-favicon');   // library for automatic handling of favicon request

        let mongoose = require('mongoose');         // library for database
        let connection = mongoose.connect('mongodb://localhost/bustracker', {useNewUrlParser: true});    // Database to query
        let inputSchema = new mongoose.Schema(
            {'_id': String, 'rt': String, 'vid': String, 'spd': String, 'tmstmp': String, 'lat': String, 'lon': String}
        );
        let inputModel = mongoose.model('buses', inputSchema);



        let app = express(); // init the framework

        let server = app.listen(4000, function () { // start the server
            let host = server.address().address;
            let port = server.address().port;
            console.log('Express server running from host ' + host + ' on port ' + port);
        });

        app.use(express.static('./webcontent')); // the folder to use to get static files (e.g. index.html)
        app.use(express.urlencoded({extended:true}));// needed when POST data is sent via application/x-www-form-urlencoded
        app.use(favicon(__dirname + '/webcontent/images/favicon.ico'));
        app.get('/', handleDefaultRoute);

        function handleDefaultRoute(request, response) {
            response.send("./webcontent/index.html");
        }

        app.get('/data', function (request, response) {
            // localhost:4000/data?a=b&c=d&e=f
            // response.writeHead(200, {'Content-Type': 'application/json'}); // not needed; automatic via response.json below!
            let data = {"status": "ok"}; // a javascript object
            data.url = request.url;
            data.name = "Stuart";
            data.major = "SE";
            response.json(data);// no need to set content-type! Express handles it automatically

        });

        app.get('/BusInfo', function (request, response) {
            let ajax = require('request'); // import the request library (downloaded from NPM) for making ajax requests
            let url = require('url');  // used for parsing request urls (in order to get params)
            let urlObject = url.parse(request.url, true); // see https://nodejs.org/api/url.html
            let route = 'GRE'; // default route if no ?rt=xxx is supplied

            if (urlObject.query["rt"]) { // check for the existence of a specific parameter
                route = urlObject.query["rt"];
            }

            let key = require("./key.js").key; // for privacy, my key is in a separate file
            let uri = "http://realtime.ridemcts.com/bustime/api/v2/getvehicles?key=" + key + "&rt=" + route + "&format=json";

            // the default response (same as asking for route 1000)
            let busData = {status: "Server Error; IOException during request to ridemcts.com: Simulated server error during request to ridemcts.com"}; // the default JSON response
            if (route === '1000') { // simulate MCTS server error...
                response.json(busData); // note that this sends the above default response
                return;
            }
            if (route === '1001') { // simulate bad ajax request
                response.status(404);
                response.end();
                // response.send({error: 'Not Found'});
                // alternate: response.status('404').send("Bad request");
                return;
            }
            if (route === '1002') { // missing key or route
                response.json({status: "Key or route parameter empty"});
                return;
            }
            if (route === '1003') { // missing key or route
                response.json({"bustime-response": {"error": [ {
                    "msg":"Invalid API access key supplied"
                } ] }});
                return;
            }

            // Finally, make the ajax call to the MCTS server:
            ajax(uri, function (error, res, body) {
                if (!error && res.statusCode === 200) {
                    busData = JSON.parse(body); // parse the JSON string to a JavaScript object
                }
                response.json(busData); // no need to set content-type! Express handles it automatically
            });
        });

        app.get('/BusSpeed', function (request, response) {
            let speed = request.query.spd;
            inputModel.find( {spd: {$gte: speed}} )
                .then(
                    function( records) { // promise resolved
                        response.status(200);
                        response.json(records);
                    },
                    function(err) { // promise rejected
                        console.error("find error: " + err );
                    }); // End find.then
        }); // End get /BusSpeed

    }// end serve() method
} // end ExpressDemo class

ExpressDemo.serve(); // alternate when using static run() method