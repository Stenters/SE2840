// normally, this would be in a separate file (jsxCourselist.js or similar)
class Courselist extends React.Component {
  render() {
    return React.createElement("ul", {
      className: "list-group"
    }, // Javascript in the middle of JSX needs to be delimited with {}
    this.props.items.map((course, index) => React.createElement("li", {
      key: index,
      className: "list-group-item list-group-item-danger"
    }, course.id + ' : ' + course.desc)));
  } // end render()


} // normally, this would be in a separate file (jsxApproach.js or similar)


class jsxApproach {
  constructor() {}

  static start() {
    $().ready(() => jsxApproach.onLoad());
  } // onLoad handling is usually used to perform initialization


  static onLoad() {
    // simulated response from Ajax call:
    let track = {
      curriculum: 'se',
      year: 'sophomore',
      term: 'winter',
      courses: [{
        id: 'se2030',
        desc: 'web app dev'
      }, {
        id: 'cs2711',
        desc: 'computer org'
      }, {
        id: 'ph2020',
        desc: 'physics'
      }, {
        id: 'MA xxx',
        desc: 'math elective'
      }]
    }; // When the button is pressed, display the response

    $('#button').click(() => {
      $('#program').html(track.curriculum);
      $('#year').html(track.year);
      $('#quarter').html(track.term); // This tells React where to render the virtual DOM structure

      ReactDOM.render(React.createElement(Courselist, {
        items: track.courses
      }), document.getElementById("courselist"));
    }); // end click()
  } // end onLoad


} // end reactApproach


jsxApproach.start();
