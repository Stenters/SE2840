/**
 * A simple NodeJS application.
 */
// import mc from './MyClass.js'; // still experimental in 2019!
class NodeJSDemo {
    // Note:
    // The constructor is called when the 'new' statement
    // (at bottom of file) is executed.
    constructor() {
        // this.run(); // alternate
    }

    // This function can have any name you choose.
    // There is nothing special about "run".
    // We could have also declared the 'run' method below to be static, and started
    // the execution via a call to that method (see comment at bottom of file).
    // static run() { // alternate
    run() {
        console.log("NodeDemo is running...");
        // let x = []; // empty array; next add some items to it...
        // x.push(1);
        // x.push("a");
        // x.push({mark: 123});
        // x.push({'se-2840': "hello"});
        // console.log("x is: " + x);
        // console.log("x[2].mark is: " + x[2].mark);
        // console.log("x[3].se-2840 is: " + x[3]['se-2840']);

        //We can also "import" Javascript objects directly from other files...
        // let mc = require('./MyClass.js');
        // mc.doSomething();
        let coinflipData = require("./CoinFlipData.js");
        console.log("coinflipData[0].name = " + coinflipData[0]);

        let fs = require('fs');  // 'import' the fs NodeJS core class
        let text = fs.readFileSync('Hello.txt', 'utf8'); // load the file into a text string
        console.log("Hello.txt contains: " + text);

        let text2 = fs.readFileSync('Student.txt', 'utf8'); // load the file into a text string
        console.log("Student.txt contains: " + text2);

        // use the built-in JSON class to magically parse a JSON string into a Javascript object
        let jsObject = JSON.parse(text2); // if 'text2' contains valid JSON, this will parse it into a Javascript object
        console.log("jsObject.student.name = " + jsObject.student.name);


        let jsontext = JSON.stringify(coinflipData); // if we have a Javascript object, this will turn it into a valid JSON string
        console.log(jsontext);

        let List = require("collections/list"); // "imports" the collections/list module we downloaded from NPM
        let list = new List([1, 2, 3]); // initialize the list and add some items to it...
        list.add(4);
        list.add(5);
        let ar = list.toArray();
        console.log(ar);
        list.reverse();
        ar = list.toArray();
        console.log(ar);
    } // end run() method
} // end NodeDemo class

let jsdemo = new NodeJSDemo(); // instantiation causes run() method to be called
jsdemo.run();
// NodeJSDemo.run(); // alternate when using static run() method