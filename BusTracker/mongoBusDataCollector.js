/**
 * Created by hornick on 5/10/2016.
 * To use mongo:
 * 1. install MongoDB (Community Edition); see https://docs.mongodb.com/manual/tutorial/install-mongodb-on-windows/
 * 2. create folder for mongo db; e.g. C:\mongo\data
 * 3. start mongo server: "C:\Program Files\MongoDB\Server\4.0\bin\mongod.exe" --dbpath c:\mongo\data
 * 4. optional: run mongo shell to interactively access db: "C:\Program Files\MongoDB\Server\4.0\bin\mongo.exe"
 *    use "help" at command prompt for available commands (e.g. show dbs, use bustracker, db.buses.drop(); db.dropDatabase(), etc
 */

class MongoBusDataCollector {
    // This application polls retrieves MCTS data periodically and saves the bus data
    // to a mongo database for subsequent analysis.

    // Note:
    // The constructor is called when the 'new' statement
    // (at bottom of file) is executed.
    constructor() {
        // do nothing in this case (never runs since this class is not instantiated)
    }

    // This function can have any name you choose.
    static start() {
        console.log("MongoBusDataCollector running.");

        let mongoose = require('mongoose'); // From NPM: MongoDB API for NodeJS
        let key = require('./key.js').key; // MCTS key

        // connect to the database named "bustracker" on the local mongo server
        let connection = mongoose.connect('mongodb://localhost/bustracker', {useNewUrlParser: true});

        // A schema describes the format/structure of the documents in a collection
        let busSchema = new mongoose.Schema(
            {
                'rt': String,
                'vid': String,
                'spd': String,
                'tmstmp': String,
                'lat': String,
                'lon': String
            }
        );

        // A model represents a document in a database collection
        let BusModel = mongoose.model('Bus', busSchema);

        setInterval(getBusData, 15000); // periodically sample bus data from MCTS

        function getBusData() {
            let ajax = require('request'); // import the request library (downloaded from NPM) for making ajax requests
            let route = "21";
            let uri = "http://realtime.ridemcts.com/bustime/api/v2/getvehicles?key=" + key + "&rt=" + route + "&format=json";

            ajax(uri, function (error, res, body) {
                // console.log('ajax request made! error= ' + error );
                // console.log('ajax request made! response= ' + res );
                // console.log('ajax request made! body= ' + body );
                if (!error && res.statusCode === 200) {
                    console.log('Checking bus speeds...');
                    let busData = JSON.parse(body); // parse the JSON string to a JavaScript object
                    for (let index in busData['bustime-response'].vehicle) {
                        let vehicle = busData['bustime-response'].vehicle[index];
                        console.log('checking bus' + index + ':' + vehicle.vid + ' ...');
                        if (vehicle.spd > 25) { // pay attention to only speeds > 25mph
                            console.log('bus' + index + ':' + vehicle.vid + ' was caught: spd= ' + vehicle.spd);
                            let bus = new BusModel( // create a new BusModel object
                                {
                                    'rt': vehicle.rt,
                                    'vid': vehicle.vid,
                                    'spd': vehicle.spd,
                                    'tmstmp': vehicle.tmstmp,
                                    'lat': vehicle.lat,
                                    'lon': vehicle.lon
                                });
                            bus.save(function (err) { // save the BusModel object to Mongo to a collection named "buses"
                                if (err !== null) {
                                    console.error(err); // log an ajax error if one occurs
                                }
                            });
                            // To retrieve entries, use (for example) db.buses.find( {'spd': {$gte:'40'}} )
                        }
                    }

                }
            });
        }
    } // end start()
} // end class MongoBusDataCollector

MongoBusDataCollector.start();

