import java.util.Scanner;

/******************************************************************************************************************
 * CoinFlipper.java
 * Adapted from Dean & Dean, ch 9.
 *
 * This program generates a histogram of coin flips. Suppose you have 5 coins and flip them 100000 times. 
 * Some flips will result in all heads, some with all tails, and others with some combination of heads and tails.
 * The histogram displays the number of times each combination occurred over all flips.
 ******************************************************************************************************************/
public class CoinFlipper {
    private int numberOfCoins;			// number of coins to flip; initialized in init() method below
    private int numberOfRepetitions;	// repetitions, or the number of times the coins are flipped; initialized in init() method below
    // The frequency array holds the number of times a particular number of heads occurred:
    // frequency[0] holds the number of times 0 heads occurred
    // frequency[1] holds the number of times 1 head occurred
    // ...
    // frequency[NUM_OF_COINS] holds the number of times all heads occurred
    private int[] frequency; // initialized in init() method below

    /**
     * Kicks off the execution of the program
     * @param args not used
     */
    public static void main(String[] args) {
        CoinFlipper coinFlipper = new CoinFlipper(); // should really use a factory to limit creation
        coinFlipper.init();
    }

    /**
     * Constructor
     * does nothing intentionally
     */
    public CoinFlipper() {
    }

    /**
     * Gathers input from the user,
     * controls execution, and measures how long it takes to execute.
     */
    public void init() {
        numberOfCoins = 0;
        numberOfRepetitions = 0;
        Scanner kbd = new Scanner(System.in);
        boolean validInput = false;
        while( !validInput ) {
            try {
                System.out.print("Enter the number of coins to be flipped: ");
                numberOfCoins = kbd.nextInt();
                System.out.println("Enter the number of flips: ");
                numberOfRepetitions = kbd.nextInt();
                frequency = new int[numberOfCoins+1];  // allocate the array per user-specified input (note: Java initializes the contents to 0).
            } catch( Exception e ) {
                System.out.println("Invalid input. Try again.");
                kbd.nextLine(); // consume the junk that was not an int
                continue;
            }
            validInput = true;
        }

        long executionTime = System.currentTimeMillis(); // current system time snapshot
        flipCoins(); // flip NUM_OF_COINS coins NUM_OF_REPS times
        executionTime = System.currentTimeMillis()-executionTime; // compute elapsed time

        // NOTE: Do not include histogram generation in execution time calculation - console I/O takes a LONG time compared to internal math computations
        printHistogram(); // display the resulting histogram

        System.out.println("Coin Flipper Time: " + executionTime + "ms");
    }

    /**
     * This method flips a specified number of coins a specified number of times,
     * and gathers the number of times a certain number of heads occurred in each flip into the frequency[] array.
     */
    private void flipCoins() {
        // This loop fills up the frequency bins. Each iteration simulates one group of numCoins coin flips.
        // For example, with a group of flips of 3 coins, heads may come up 0, 1, 2, or 3 times.
        for( int rep=0; rep<numberOfRepetitions; rep++ ) {
            // perform a single flip of NUM_OF_COINS coins
            int heads = doSingleFlip();
            frequency[heads]++; // update appropriate bin
        }
    }

    /**
     * This method flips a specified number of coins and returns the number heads that occurred in the flip.
     * It makes use of a random number generator to randomly generate heads or tails for each coin flipped.
     * @return the number of heads that occurred in the flip
     */
    private int doSingleFlip() {
        int heads = 0;
        for( int i=0; i<numberOfCoins; i++ ) { // flip each coin
            heads += (int)(Math.random() * 2); // computed random int value is either 0 or 1 (tails or heads)
        }
        return heads; // number of heads that came up
    }

    /**
     * This method prints a histogram of the number of heads that occurred for a specified number of flips
     * Notes: The output generated for numCoins=5 and numReps=100000 may look something like this:
     *
     * Number of times each head count occurred in 100000 flips of 5 coins:
     * 0  3076  ***
     * 1  15792  ****************
     * 2  31348  *******************************
     * 3  31197  *******************************
     * 4  15552  ****************
     * 5  3035  ***
     */
    private void printHistogram() {
        System.out.println("Number of times each head count occurred in " + numberOfRepetitions + " flips of " + numberOfCoins + " coins:");

        // This loop prints the histogram. Each iteration prints one histogram bar.
        for( int heads=0;heads<=numberOfCoins; heads++ ) {
            System.out.print( " " + heads + "  " + frequency[heads] + "  " );
            double fractionOfReps = (float) frequency[heads] / numberOfRepetitions;
            int numOfAsterisks = (int) Math.round(fractionOfReps * 100);

            for( int i=0; i<numOfAsterisks; i++ ) {
                System.out.print("*");
            }
            System.out.println();
        }
    }
}