// Stuart Enters
// SE20840
// Dec. 5, 2018
// Hornick


// JS6 class approach to objects
// In Webstorm: in File/Settings, under Languages and Frameworks/Javascript, set language version to ES 6
//              in Code/Configure Current File Analysis..., follow Configure Inspections link. Select Javascript and then select analysis tool to use.
// Tell the code inspection tool that we're writing ES6 compliant code:
// jshint esversion: 6
// jshint devel: true
// See https://jshint.com/docs/ for more JSHint directives

let numAsterisks = 100;

class CoinFlipper {

    /**
     * Contructor left blank intentionally
     */
    constructor() {
    }

    /**
     * Method for flipping x coins y times. Output is printed to console
     * @returns {Promise<void>} ignored
     */
    async init(){
        // Clear the console from any previous session
        window.console.clear();

        // Get the desired number of coins & flips and instantiate an array to store results of coin flips
        let numCoins = this.getNum("Enter the number of coins to be flipped: ");
        let numFlips = this.getNum("Enter the number of times to flip the coins: ");
        // let numCoins = 10;
        // let numFlips = 1000000;
        let frequency = new Array(numCoins + 1).fill(0);

        // Execute the body, while recording execution time
        let startTime = Date.now();
        this.flipCoins(numCoins, numFlips, frequency);
        let endTime = Date.now();
        let deltaTime = endTime - startTime;

        // Print results to console
        this.printHistogram(numCoins, numFlips, frequency, deltaTime);

        // await this.sleep(3000);
        // location.reload();
    }

    /**
     * Helper function for quering the user for a number
     * @returns {number} the number
     */
    getNum(message){
        let validInput = false; // Default imput to invalid (NaN)
        let num = 0;

        // Repeatedly query user for number until you get one
        while (!validInput) {
            num = prompt(message);

            if (isNaN(num)) {
                alert("Please enter a valid number");

            } else if (num < 1){
                alert("Please enter a positive number");

            } else {
                validInput = true;  // Number was entered
            }
        }
        return parseInt(num);
    }

    /**
     * Method for fliping muliple coins
     * @param numCoins the number of coins to flip
     * @param numFlips the number of times to flip each coin
     * @param frequency the array to save the number of heads each coin flipped
     */
    flipCoins(numCoins, numFlips, frequency) {

        // For each flip, count how many heads there are
        for (let i = 0; i < numFlips; i++) {
            let numHeads = 0;

            // Flip each coin 1 time
            for (let j = 0; j < numCoins; j++) {
                numHeads += this.doSingleFlip();
            }

            frequency[numHeads]++;   // Save result to array
        }
    }

    /**
     * Helper method for flipping a single coin
     * @returns {number} a 0 for tails, a 1 for heads
     */
    doSingleFlip() {
        return Math.floor(Math.random() * 2);   // Generate a number between 0 and 2
                                                   // and floor it to be either 0 or 1
    }

    /**
     * Method for printing out a histogram of coin flip results
     * @param numCoins the number of coins flipped
     * @param numFlips the number of times each coin was flipped
     * @param frequency an array containing the results of the trial
     * @param deltatime the execution time of the trial
     */
    printHistogram(numCoins, numFlips, frequency, deltatime) {

        // Log results
        window.console.log("Flipping " + numCoins + " coins " + numFlips +
            " times...");

        // For each coin, print out results
        for (let i = 0; i < frequency.length; i++){
            this.log(i, frequency[i], numFlips);  // Method for formatting output
        }

        // Print out execution time of program
        console.log("This took " + deltatime + "ms to complete");
    }

    /**
     * Helper method for formatting a single log entry of the histogram
     * @param coin the coin to log
     * @param numHeads the number of times the coin flipped heads
     * @param numFlips the number of times the coin was flipped
     */
    log(coin, numHeads, numFlips) {
        let fraction = numHeads / numFlips;                     // The fraction of this coin's heads to the total count
        let asterisks = Math.round(fraction * numAsterisks); // How many of <numAsterisks> to allocate
        let out = (coin + ": " + numHeads);                     // Initialize the output

        for (let j = 0; j < asterisks; j++) {
            out += "*";
        }

        console.log(out);
    }

    /**
     * Unused method for sleeping the thread
     * @param ms the time to sleep the thread (in milliseconds)
     * @returns {Promise<event?>} timeout on the thread
     */
    sleep(ms){
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    /**
     * Unused method for iterating through an array and applying a function to each item
     * @param array the array to iterate through
     * @param func the function to apply
     * @returns {*} (optional) the return of the function
     */
    foreach(array, func){
        let optional;
        for (let i = 0; i < array.length; i++){
            optional += func(i, array[i]);
        }
        return optional;
    }
}
