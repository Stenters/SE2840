function poisson (l, x) {
    let a = Math.pow(l,x);
    let b = Math.pow(Math.E, -l);
    let c = factorialize(x);
    return (a * b) / c;
}

function factorialize(num) {
    if (num < 0)
        return -1;
    else if (num === 0)
        return 1;
    else {
        return (num * factorialize(num - 1));
    }
}