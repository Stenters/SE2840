class MidtermHandler{
    constructor(){
        window.onload = () => this.onLoad();
    }

    onLoad() {
        let valInput = $('#value');
        let threshInput = $('#threshold');
        let table = $('#entries');
        let msgDiv = $('#message');
        let total = $('#total');
        let allValid = [];

        msgDiv.hide();
        total.hide();
        $('.headings').text("Stuart Enters");
        $('#enter').get(0).onclick = () => checkVal();
        $('#sum').get(0).onclick = () => sum();


        function checkVal() {
            let valid = true;
            let valInputText = valInput.val();

            if (valInputText === "") {
                writeErr("Value cannot be empty")
            } else {

                if (valInputText === undefined || valInputText === null || isNaN(valInputText)) {
                    valid = false;
                } else {
                    allValid.push(Number(valInputText));
                }

                let markup = "<tbody><tr class='row'> <td>" + valInputText + "</td> <td>" + valid + "</td></tr></tbody>";

                table.append(markup);
            }

        }

        function sum() {
            let threshText = threshInput.val();

            if (threshText === "") {
                writeErr("Threshold cannot be emty");
            } else {

                if (threshText === undefined || threshText === null || isNaN(threshText)) {
                    writeErr("Threshold must be a numeric");
                } else {
                    let threshold = Number(threshText);
                    let sum = 0;

                    for (let n in allValid) {
                        if (allValid[n] > threshold) {
                            sum += allValid[n];
                        }
                    }
                    total.text("Sum of values larger than " + threshold + " is " + sum);

                }
            }
        }

        function writeErr(errMsg) {
            msgDiv.show();
            msgDiv.text(errMsg);
            msgDiv.fadeOut(2500);
        }

    }
}