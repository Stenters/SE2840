/**
 * Stuart Enters
 * SE20840
 * Dec. 18, 2018
 * Hornick
 * @type {CoinFlipper}
 */

// jshint esversion: 6
// jshint devel: true
// See https://jshint.com/docs/ for more JSHint directives

/**
 * Class for simulating flipping n coins m times
 * And printing the results using DOM
 */
class CoinFlipper {

    /**
     * Constructor left intentionally blank
     */
    constructor(){}

    /**
     * Method for initializing the coin flipper
     */
    init(){
        document.getElementById("histogramTable").innerHTML = "";


        // Get the number of coins from the user and check it is valid
        let numCoins = document.getElementById("numCoinsInput").value;
        let numCoinsErrText = document.getElementById("numCoinsErrorText");
        let isValidCoin = this.sanitize(numCoins, numCoinsErrText);
        numCoins = parseInt(numCoins);

        // Make sure the number of coins isn't greater than 10
        if (isValidCoin && numCoins > 10){
            this.logErr("Please enter a number between 1 and 10", numCoinsErrText);
            isValidCoin = false;
        }

        // Get the number of flips from the user and check it is valid
        let numFlips = document.getElementById("numFlipsInput").value;
        let numFlipsErrText = document.getElementById("numFlipsErrorText");
        let isValidFlip = this.sanitize(numFlips, numFlipsErrText);
        numFlips = parseInt(numFlips);

        // Using the valid input, begin coin flipping
        if(isValidCoin && isValidFlip){
            numCoinsErrText.innerHTML = "";
            numFlipsErrText.innerHTML = "";

            this.runCoinFlipper(numCoins, numFlips);
        }
    }

    /**
     * Helper method for validating the user's input
     * @param input the input to validate
     * @param errTextLocation where to report any error to
     * @returns {boolean} if the input was valid
     */
    sanitize(input, errTextLocation) {
        let isValid = true;

        // If input is empty, fail
        if (input === "") {
            this.logErr("Please enter a value", errTextLocation);
            isValid = false;

        // If input is not a number, fail
        } else if (isNaN(input)) {
            this.logErr("Please enter a number", errTextLocation);
            isValid = false;

        // If input is 0 or negative, fail
        } else if (input < 1) {
            this.logErr("Please enter a positive number", errTextLocation);
            isValid = false;

        }
        return isValid;
    }

    /**
     * Helper method for logging given error msg in given textArea
     * @param errMsg the message to log
     * @param textArea the textArea to log the message in
     */
    logErr(errMsg, textArea) {
        textArea.innerHTML = errMsg;
    }

    /**
     * Method for calling the helper functions
     * @param numCoins the number of coins to flip
     * @param numFlips the number of times to flip
     */
    runCoinFlipper(numCoins, numFlips) {
        // Initialize an array to store results in
        let frequency = new Array(numCoins + 1).fill(0);

        // Flip all the coins and time the results
        let time = Date.now();
        this.flipCoins(numCoins, numFlips, frequency);
        time = Date.now() - time;

        // Draw the results using DOM
        this.draw(frequency, time, numFlips);
    }

    /**
     * Method for flipping multiple coins
     * @param numCoins the number of coins to flip
     * @param numFlips the number of times to flip each coin
     * @param frequency the array to save the number of heads each coin flipped
     */
    flipCoins(numCoins, numFlips, frequency) {

        // For each flip, count how many heads there are
        for (let i = 0; i < numFlips; i++) {
            let numHeads = 0;

            // Flip each coin 1 time
            for (let j = 0; j < numCoins; j++) {
                numHeads += this.doSingleFlip();
            }

            frequency[numHeads]++;   // Save result to array
        }
    }

    // Improved over previous iteration, return either a 0 or 1
    doSingleFlip() {
        return Math.round(Math.random());
    }

    /**
     * Method for drawing results to screen
     * @param frequency the array containing the results
     * @param time the time it took to run
     * @param numFlips the number of times it was flipped
     */
    draw(frequency, time, numFlips) {
        // Get the empty table from the HTML
        let histogram = document.getElementById("histogramTable");
        histogram.innerHTML = "";   // Ensure there is no previous data in the histogram
        for (let i = 0; i < frequency.length; i++){

            let max = Math.max.apply(null, frequency);
            let scalingFactor = 100 * (max / numFlips);

            //  Iterate through all the table rows, creating them one by one
            // Create the new table row
            let tr = this.append('tr', histogram);

            // Get the number of heads this row represents
            let count = this.append('td', tr);
            count.appendChild(document.createTextNode(i));

            // Get the number of times this number of heads occurred
            let occurrence = this.append('td', tr);
            occurrence.appendChild(document.createTextNode(frequency[i]));

            // Create the progress bar
            let progressBar = this.append('progress', tr);
            progressBar.id = "progressBar";
            progressBar.max = scalingFactor;
            progressBar.value = Math.round((frequency[i] / numFlips) * 100);

            // Apply .css style guides to the elements
            count.style.width = "10%";
            occurrence.style.width = "10%";
            progressBar.style.width = "100%";
        }

        // Create a label to report the time taken
        let timer = this.append('label', histogram);
        timer.appendChild(document.createTextNode("Elapsed time: " + time + " ms"));
    }

    /**
     * Helper method for creating a desired child element and attaching it to a parent
     * @param child the child to create
     * @param parent the parent to append the child to
     * @returns {HTMLElement} the created child element
     */
    append(child, parent) {
        let childElement = document.createElement(child);
        parent.appendChild(childElement);
        return childElement;
    }
}
