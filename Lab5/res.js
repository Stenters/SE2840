let res = {
    "bustime-response": {
        "vehicle": [
            {
                "vid": "5517",
                "tmstmp": "20190109 20:38",
                "lat": "43.06016286214193",
                "lon": "-88.04241180419922",
                "hdg": "87",
                "pid": 10353,
                "rt": "31",
                "des": "Downtown",
                "pdist": 4892,
                "dly": false,
                "spd": 31,
                "tatripid": "3741",
                "tablockid": "31 -303",
                "zone": ""
            },
            {
                "vid": "5513",
                "tmstmp": "20190109 20:38",
                "lat": "43.04794799449832",
                "lon": "-88.0022279606309",
                "hdg": "294",
                "pid": 10401,
                "rt": "31",
                "des": "Innovation Drive",
                "pdist": 28617,
                "dly": false,
                "spd": 18,
                "tatripid": "919",
                "tablockid": "31 -304",
                "zone": ""
            },
            {
                "vid": "5527",
                "tmstmp": "20190109 20:38",
                "lat": "43.03544333333333",
                "lon": "-87.91758666666666",
                "hdg": "182",
                "pid": 10418,
                "rt": "31",
                "des": "Downtown",
                "pdist": 48376,
                "dly": false,
                "spd": 0,
                "tatripid": "2927",
                "tablockid": "31 -351",
                "zone": ""
            }
        ]
    }
};

