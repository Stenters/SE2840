/**
 * Stuart Enters
 * Hornick
 * Lab 5: Bus Tracker
 * 1/15/18
 */

class BusTracker {
    // Note: We could also have used a static Factory Method (as in previous code examples) to call the constructor.
    constructor() {
        $(document).ready(function() {      // when document loads, do some initialization
            onLoad();
        });

        // Note: Any "private" variables you create via "let x=..." will be visible to the "onload" function below and its
        // nested inner functions. (You probably don't need to declare any extra variables however).

        // The onLoad "private" member function is called when the document loads and is used to perform initialization.
        let onLoad = function() {
            // Note: these local vars will be visible/accessible within inner functions below!
            let map = null;	        // a Google Map object
            let timer = null;       // an interval timer
            let update = 0;         // an update counter
            let markerList = [];    // a list of all the markers

            let startPosition = new google.maps.LatLng(43.044240, -87.906446);// location of MSOE athletic field
            createMap(startPosition); // map this starting location (see code below) using Google Maps
            addDefaultMarker();  // add a push-pin to the map

            // initialize button event handlers (note this shows an alternative to $("#id).click(handleClick)
            $("#start").on( "click", doAjaxRequest);
            $("#stop").on( "click", stopTimer);

            //NOTE: Remaining helper functions are all inner functions of onLoad; thus, they have
            // access to all vars declared within onLoad.

            // Create a Google Map centered on the specified position. If the map already exists, update the center point of the map per the specified position
            // param position - a google.maps.LatLng object containing the coordinates to center the map around

            function createMap(position) {
                let mapOptions = {
                    zoom: 13, // range 0 to 21 (the mouse can be used to zoom in and out)
                    center: position, // the position at the center of the map
                    mapTypeId: google.maps.MapTypeId.ROADMAP // ROADMAP, SATELLITE, or HYBRID
                };
                let mapDiv = $("#map"); // get the DOM <div> element underlying the jQuery result
                map = new google.maps.Map(mapDiv.get(0), mapOptions); // create the google map
                map.panTo(position); // pan the map to the specified position
                mapDiv.css({"width": "80%", "height": "600px"});
            }
            // This function adds a "push-pin" marker to the existing map
            // param map - the map to add the marker to
            // param position - the google.maps.LatLng position of the marker on the map
            // param title - the title of the marker
            // param content - the text that appears when a user clicks on the marker

            function addMarker(position, title, content) {
                let markerOptions = {
                    position: position, // position of the push-pin
                    map: map,	// the map to put the pin into
                    icon: './res/busPinImg1.png',
                    title: title, // title of the pin
                    clickable: true // if true, enable info window pop-up
                };
                // create the push-pin marker
                let marker = new google.maps.Marker(markerOptions);
                markerList.push(marker);
                console.log(typeof marker);

                // now create the pop-up window that is displayed when you click the marker
                let infoWindowOptions = {
                    content: content, // description
                    position: position // where to put it
                };
                let infoWindow = new google.maps.InfoWindow(infoWindowOptions);

                google.maps.event.addListener(marker, "click", function() {
                    infoWindow.open(map);
                });
            } // end inner function addMarker

            // Function for adding MSOE to the map
            function addDefaultMarker() {
                let startPosition = new google.maps.LatLng(43.044240, -87.906446);// location of MSOE athletic field

                let markerOptions = {
                    position: startPosition, // position of the push-pin
                    map: map,	// the map to put the pin into
                    title: 'MSOE', // title of the pin
                    clickable: true // if true, enable info window pop-up
                };
                // create the push-pin marker
                let marker = new google.maps.Marker(markerOptions);
                markerList.push(marker);
                console.log(typeof marker);

                // now create the pop-up window that is displayed when you click the marker
                let infoWindowOptions = {
                    content: "The Place to Be!", // description
                    position: startPosition // where to put it
                };
                let infoWindow = new google.maps.InfoWindow(infoWindowOptions);

                google.maps.event.addListener(marker, "click", function() {
                    infoWindow.open(map);
                });
            }

            // This function executes a JSON request to the CPULoadServlet
            function doAjaxRequest() {
                // Read the MCTS key, and the desired route the user entered
                let params ={key : key, rt : $("#route").val()};

                $.ajax({
                    type: "GET",
                    url : "http://sapphire.msoe.edu:8080/BusTrackerProxy/BusInfo", // the url of the servlet returning the Ajax response
                    data : params, // key and route, for example "key=ABCDEF123456789&rt=31"
                    async: true, // when true, use a worker thread (using false is deprecated!)
                    crossDomain: true, // true tells the server we're making a cross-domain request (which may not be honored)
                    dataType: "json", // we're getting a JSON response containing the data
                    success: handleSuccess, // the function to call on success
                    error: handleError // the function to call if an error occurs
                });

                // If the timer is null, call doAjaxRequest every 5 sec, and update how many times it's been called
                if (timer === null) {
                    update = 1;
                    $('#update').text(update);
                    timer = window.setInterval(function () {
                        $("#update").text(++update);
                        doAjaxRequest();
                    }, 5000)
                }

            }// end inner function doAjaxRequest

            // This function stops the timer and nulls the reference
            function stopTimer() {
                dispErr("Timer stopped!");
                window.clearInterval(timer);
                timer = null;
                $('#update').text("");
                clear();    // Clear the markers from the map
            }// end inner function stopTimer



            // Note that the Ajax request can succeed, but the response may indicate an error (e.g. if a bad route was specified)
            function handleSuccess( response, textStatus, jqXHR ) {
                hideErr();  // Hide any previous error messages
                let table = $("#table1");
                table.html("");

                // If the ajax request went through but didn't succeed, record the status
                if (!textStatus.includes("success")){
                    dispErr("The status of the response was " + textStatus);
                    console.log("textStatus was: " + textStatus);

                // If the status wasn't 200 OK, print the status message
                } else if (jqXHR.status !== 200) {
                    dispErr("Recived error code of " + jqXHR.status + " with description " + jqXHR.statusMessage);
                    console.log("More info:\n" + JSON.stringify(jqXHR, null, 4));

                // If the expected JSON object isn't there, display a warning
                } else if (response["bustime-response"] === undefined) {

                    if (response["status"].includes("Server Error")) {
                        dispErr("There was a Server Error , please come back later");
                    } else {
                        dispErr("Error: " + response["status"]);
                    }

                    console.log(response["status"]);

                // If the JSON object doesn't contain a vehicle array, print out an error
                } else if (response["bustime-response"].vehicle === undefined) {
                    let err = response["bustime-response"].error[0];

                    if (err.msg.includes("Invalid API access key")){
                        dispErr("Invalid key supplied, please contact MCTS for another");

                    } else {
                        dispErr("There was no data at route: " + response["bustime-response"].error[0].rt + "." +
                            "\tPlease make sure a valid route was entered");

                    }
                    console.log("Error: " + response["bustime-response"].error[0].msg);

                // AJAX request succeeded!
                }else {
                    // Initialize the table
                    let innerhtml = "<thead><tr><th>Bus</th><th>Route " + response["bustime-response"].vehicle[0].rt + "</th><th>Latitude</th><th>Longitude</th><th>Speed (MPH)</th><th>Dist (mi)</th></tr></thead><tbody>";

                    // For every bus, append its data to the table
                    // and create a Google pushpin
                    for (let i = 0; i < response["bustime-response"].vehicle.length; i++) {
                        let vehicle = response["bustime-response"].vehicle[i];

                        innerhtml += "<tr>";
                        innerhtml += "<td>" + vehicle.vid + "</td>";
                        innerhtml += "<td>" + vehicle.des + "</td>";
                        innerhtml += "<td>" + vehicle.lat+ "</td>";
                        innerhtml += "<td>" + vehicle.lon + "</td>";
                        innerhtml += "<td>" + vehicle.spd + "</td>";
                        innerhtml += "<td>" + vehicle.pdist + "</td>";
                        innerhtml += "</tr>";

                        let position = new google.maps.LatLng(vehicle.lat, vehicle.lon);
                        addMarker(position, vehicle.vid, vehicle.des);
                    }

                    innerhtml += "</tbody>";
                    table.html(innerhtml);
                }
            }// end inner function handleSuccess

            function clear() {
                for (let marker in markerList){
                    markerList[marker].setMap(null);
                }
                addDefaultMarker();
                $("#table1").html("");
            }

            function partial_clear(num) {
                let size = markerList.length / (num + 1);
                for (let i = 0; i <= size; i++){
                    markerList[i].setMap(null);
                }
            }

            function dispErr(message) {
                $("#errHeader").text(message);
            }

            // This function is called if the Ajax request succeeds.
            function hideErr() {
                $("#errHeader").text("");
            }

            // This function is called if the Ajax request fails (e.g. network error, bad url, server timeout, etc)
            function handleError(jqXHR, textStatus, errorThrown) {
                dispErr("There was a error when communicating with the server. Error was: " + errorThrown );
                console.log("Err. " + errorThrown + " thrown by website. Details:");
                console.log("Status: " + jqXHR.status + " with message " + jqXHR.statusMessage);

            } // end inner function handleError

        }; // end onLoad "private" method

    }// end "public" constructor

} // end class BusTracker