let c = 299792458;

function delt_t_0(delt_t, v) {
    let ans = Math.pow(v,2) / Math.pow(c,2);
    ans = Math.sqrt(1 - ans);
    return delt_t * ans;
}

function v(delt_t, delt_t0) {
    let ans = delt_t0 / delt_t;
    ans = Math.pow(ans, 2);
    ans = 1 - ans;
    ans = ans * Math.pow(c, 2);
    return Math.sqrt(ans);
}

function delt_t(delt_t0, v) {
    let ans = Math.pow(v, 2) / Math.pow(c, 2);
    ans = 1 - ans;
    return delt_t0 / Math.sqrt(ans);
}