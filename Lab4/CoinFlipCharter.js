class CoinFlipCharter {
    constructor() {
        window.onload = () => this.onLoad();
    } // end constructor

// This member function is invoked when the document has finished loading.
    onLoad() {

        let context; // the graphics context of the canvas; similar to a swing ContentPane
        let canvas;  // the canvas element; similar to a java swing JFrame

        let chart;   // the chartjs object (assuming you're using chartjs)
        let chartData;  // data object to be supplied to the chart (if using chartjs)

        let table = $("#table1");   // The table showing the data
        let filter = $("#filter");

        // Note: You may add additional variables if needed, but be sure to document them.
        canvas = $('#chart1')[0];

        context = canvas.getContext('2d');
        chart = new Chart(context, {
            type: 'bar',
            data: results,
            options: {}
        }); // initialize the chart

        chart.defaults = Chart.defaults;
        chart.defaults.global.responsive = true; // make the chart responsive

        let button = $("#update").get(0);
        button.onclick = () => updateDisplay();

        createDefaultDisplay();

        filter.keypress(function (event) {
            let keypress = event.keyCode ? event.keyCode : event.witch;
            if (keypress === 13){
                event.preventDefault();
                updateDisplay();
            }
        });


// This inner function creates the "default" page.
// Note: You may add parameters to this function if needed, but be sure to document them.
        function createDefaultDisplay() {
            // Initialize the "data" object used by the Chartjs chart
            let times = [];
            let lables = [];
            // Initialize the inner html of the table to "", and then iterate through the result set, adding
            // table rows and table data
            table.html("<tbody> ");
            for (let i = 0; i < results.length; i++){
                let markup = "<tr class='row'> <td>" + i + "</td> " +
                    "<td id='Name'> " + results[i].name + " </td> " +
                    "<td id='Time'> " + results[i].time + " </td> " +
                    "<td id='IP'> " + results[i].ip + " </td> " +
                    "<td id='Session'> " + results[i].sessionid + " </td>";
                table.append(markup);

                times.push(results[i].time);
                lables.push(i + 1);
            }
            table.append(" </tbody>");

            chartData = {
                labels: lables,
                datasets: [{
                    label: "Execution Times",
                    backgroundColor: 'rgb(255, 99, 132)',
                    borderColor: 'rgb(255, 99, 132)',
                    data: times,
                }]
            };

            chart.data = chartData;

            // Also display the chart.
            // Note: The Chartjs chart object supposedly supports an update() method, but I've found that it
            // doesn't seem to work as documented. This hack is another way of rebuilding the chart whenever
            // it needs to be re-created.

            if (chart !== null) // if an old chart exists, destroy it before creating a new one
                chart.destroy();
            chart = new Chart(context, {
                type: 'bar',
                data: chartData,
                options: {}
            }); // see http://www.chartjs.org/docs/#getting-started

        } // end createDefaultDisplay()

// This inner function is called whenever the Apply/Update button is pressed.
// When it is called, it applies the specified filter expression to the result set, and
// redraws the table and chart with only the filtered results.
// Note: You may add parameters to this function if needed, but be sure to document them.
        function updateDisplay() {

            let newLables = [];
            let newTimes = [];

            // Determine which radio button is currently selected.
            let field = $("input[name='selector']:checked").val();

            // Retrieve the filter expression. You'll use this to determine what rows of the result set to show and hide.
            // Refer to the documentation on JavaScript String object's methods to figure out how to use the filter
            // expression to exclude results that don't match the filter expression.

            let rows = $("#table1").children()[0].children;
            let index;

            if (field.includes("Name")){
                index = 1;
            } else if (field.includes("Time")){
                index = 2;
            } else if (field.includes("IP")){
                index = 3;
            } else if (field.includes("Session")){
                index = 4;
            } else {
                alert("Illegal state with the radio buttons");
            }

            let regex;
            let useRegex = !!$("input[name='regex']:checked").val();


            if (useRegex){
                regex = new RegExp(filter.val());

            } else {
                regex = filter.val().toLowerCase();
            }

            for(let i = 0; i < rows.length; i++) {

                // if(!rows[i].children[index].innerText.toLowerCase().includes(filter.val().toLowerCase())) {
                 if (useRegex && !regex.test(rows[i].children[index].innerText.toLowerCase())) {
                     $(rows[i]).fadeOut(500);

                } else if (!useRegex && !rows[i].children[index].innerText.toLowerCase().includes(regex)){
                     $(rows[i]).fadeOut(500);

                }else if ($(rows[i]).is(":hidden")) {
                    $(rows[i]).fadeIn(500);
                    newLables.push(i + 1);
                    newTimes.push(rows[i].children[2].innerText);

                } else {
                    newLables.push(i + 1);
                    newTimes.push(rows[i].children[2].innerText);
                }
}
            chartData = {
                labels: newLables,
                datasets: [{
                    label: "Execution Times",
                    backgroundColor: 'rgb(255, 99, 132)',
                    borderColor: 'rgb(255, 99, 132)',
                    data: newTimes,
                }]
            };

            if (chart !== null) { // if an old chart exists, destroy it before creating a new one
                chart.destroy();
            }
            chart = new Chart(context, {
                type: 'bar',
                data: chartData,
                options: {}
            }); // see http://www.chartjs.org/docs/#getting-started

            // When done iterating, set the inner html of the table and re-display a filtered chart.
            // Note that you're not actually removing data from the result set - you're only showing the filtered
            // subset. Thus, the indices of the subset should match those of the original data set.
        } // end updateDisplay()

// Note: You may add more functions if you want to, but be sure to fully document them.
    } // end onLoad()

} // end CoinFlipCharter class