class Q4 {

    constructor(thisName, thisColor) {
        this.name = thisName;
        setColor(thisColor);

        function setColor(thisColor) {
            this.color = thisColor;
        }

    }

    getName(){
        return this.name;
    }

}