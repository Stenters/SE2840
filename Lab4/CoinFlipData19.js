// Note that 'results' is a global variable.
// Not the best approach, but it keeps things simple for this assignment.
// A better approach would have been to define a 'CoinFlipData' class with a 'getResults' method.
var results =
    [
        {
            "ip": "10.206.213.150",
            "name": "Lucas LaRocco",
            "time": "195",
            "sessionid": "1A55726EAD822F23F299BAB375E2E9B9"
        },
        {
            "ip": "10.206.213.97",
            "name": "Nate Mlezvia",
            "time": "252.8",
            "sessionid": "BA36D4ED23A8DC662FF197DB7A6A13D6"
        },
        {
            "ip": "10.206.213.97",
            "name": "Nate Mleziva",
            "time": "524.65",
            "sessionid": "BA36D4ED23A8DC662FF197DB7A6A13D6"
        },
        {
            "ip": "10.206.213.97",
            "name": "Nate Mleziva",
            "time": "298.4",
            "sessionid": "BA36D4ED23A8DC662FF197DB7A6A13D6"
        },
        {
            "ip": "10.206.212.142",
            "name": "Max",
            "time": "200",
            "sessionid": "5DDFDD3A81CB08552A30DCC769858D51"
        },
        {
            "ip": "10.206.213.150",
            "name": "Lucas LaRocco",
            "time": "65",
            "sessionid": "1A55726EAD822F23F299BAB375E2E9B9"
        },
        {
            "ip": "10.206.212.120",
            "name": "Steven Fontaine",
            "time": "103",
            "sessionid": "889426E55057BCADF0E0D429D28D2B91"
        },
        {
            "ip": "10.206.212.142",
            "name": "Max",
            "time": "100",
            "sessionid": "5DDFDD3A81CB08552A30DCC769858D51"
        },
        {
            "ip": "10.206.212.120",
            "name": "Steven Fontaine",
            "time": "259",
            "sessionid": "889426E55057BCADF0E0D429D28D2B91"
        },
        {
            "ip": "10.206.212.240",
            "name": "Joe Casper",
            "time": "65",
            "sessionid": "F621B8DF39009DB89379F5C7647E2DBD"
        },
        {
            "ip": "10.206.213.30",
            "name": "Cole Alburrito",
            "time": "192",
            "sessionid": "1C82DC4FFDAB1422848E9B943C56B3B0"
        },
        {
            "ip": "10.206.213.245",
            "name": "Isaac Ballone",
            "time": "64",
            "sessionid": "364DFD8152DD444AB6CBE705EDFA6DF5"
        },
        {
            "ip": "10.206.213.184",
            "name": "Jake Evenson",
            "time": "189",
            "sessionid": "103A98D78388B83386EC1363432CBA2A"
        },
        {
            "ip": "10.206.212.103",
            "name": "Sam Forest",
            "time": "251",
            "sessionid": "98DA76ED64313FC9F2462B3AB6DB6F26"
        },
        {
            "ip": "10.206.212.134",
            "name": "Seth Fenske",
            "time": "93",
            "sessionid": "E17AC5D7E5394AE6B1E5EC56D0A5314D"
        },
        {
            "ip": "10.206.213.23",
            "name": "Steve Arrowsmith",
            "time": "2561",
            "sessionid": "41441721C182A2EAD13DDCD77EC5C810"
        },
        {
            "ip": "10.206.213.30",
            "name": "Cole Albright",
            "time": "1920",
            "sessionid": "1C82DC4FFDAB1422848E9B943C56B3B0"
        },
        {
            "ip": "10.206.212.114",
            "name": "Stuart Enters",
            "time": "237.6",
            "sessionid": "67E10F9C779AEA2B869010CEAE8577AF"
        },
        {
            "ip": "10.206.212.114",
            "name": "Stuart Enters",
            "time": "95.3",
            "sessionid": "67E10F9C779AEA2B869010CEAE8577AF"
        },
        {
            "ip": "10.206.212.218",
            "name": "David Haen",
            "time": "456",
            "sessionid": "71BCEE6A85E2C488C54F0DD92E94164B"
        },
        {
            "ip": "10.206.213.196",
            "name": "Sam Rousser",
            "time": "275",
            "sessionid": "D3ADBC23AC02AC3656F011E04C29F991"
        },
        {
            "ip": "10.206.212.114",
            "name": "Stuart Enters",
            "time": "199.4",
            "sessionid": "67E10F9C779AEA2B869010CEAE8577AF"
        },
        {
            "ip": "10.206.212.114",
            "name": "Stuart Enters",
            "time": "417.4",
            "sessionid": "67E10F9C779AEA2B869010CEAE8577AF"
        },
        {
            "ip": "10.206.212.180",
            "name": "Rob Retzlaff",
            "time": "251",
            "sessionid": "16C75593EC1DEEF6DB708AD6D260FF79"
        },
        {
            "ip": "10.206.213.223",
            "name": "Vincent Krenz",
            "time": "181",
            "sessionid": "F7C92A97255E2AE513B948537F95A9E1"
        }
    ];